<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajouter enfant</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
        <%--<script type='text/javascript' src="<c:url value="/resources/js/app.js" />"></script> --%>

    </head>

    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Ajouter enfant</h1>
        </nav>

        <div>
            <c:if test="${ !empty nom }"><p><c:out value="Bonjour, vous vous appelez ${ nom }" /></p></c:if>
            <c:if test="${ !empty prenom }"><p><c:out value="Bonjour, vous vous appelez ${ prenom }" /></p></c:if>

            <form method="post" action="ajouterEnfant">
                <label for="prenom">Quel est son prenom ?</label>
                <p><input type="text" name="prenom" /></p>


                <label for="nom">Quel est son nom ?</label>
                <p><input type="text" name="nom" /></p>

                <label for="age">Quel est son age ?</label>
                <p><input type="number" name="age" /></p>

                <label for="date"> Date ?</label>
                <input type="date" /><br />
                <br />
                <label for="time"> Time ?</label>
                <input type="time" />

                <input type="submit" value="Envoyer" />
           </form>
        </div>

        <div>
            <h1>Hello Closed World ( this page is protected ) !!!</h1>
        </div>
    </body>
</html>


