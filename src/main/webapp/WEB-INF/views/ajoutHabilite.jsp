<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajouter personnes habilitées</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
        <%--<script type='text/javascript' src="<c:url value="/resources/js/app.js" />"></script> --%>

    </head>

    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Personnes habilitées</h1>
        </nav>

        <div>
            <form method="post" action="addChild">
                <label for="prenom">Quel est son prenom ?</label>
                <p><input type="text" name="prenom" /></p>

                <label for="nom">Quel est son nom ?</label>
                <p><input type="text" name="nom" /></p>

                <label for="age">Quel est son age ?</label>
                <p><input type="number" name="age" /></p>

                <label for="tel">Quel est son numero de telephone ?</label>
                <p><input type="number" name="tel" /></p>
                <input type="submit" value="Envoyer" />
           </form>
        </div>
    </body>
</html>


