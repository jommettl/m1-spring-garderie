<link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
<div id="blocMenu">
    <ul id="menu">
        <li><a href="/accueil" class="menuElement">Accueil</a></li>
        <li><a href="/log-in" class="menuElement">Mon compte</a></li>
        <li><a href="/info" class="menuElement">Informations</a></li>
        <li><a href="/calendar" class="menuElement">Calendrier</a></li>
    </ul>
</div>
