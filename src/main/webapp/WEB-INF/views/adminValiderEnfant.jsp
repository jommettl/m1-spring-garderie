<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Valider un enfant</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
        <%--<script type='text/javascript' src="<c:url value="/resources/js/app.js" />"></script> --%>
    </head>

    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Admin valider enfant</h1>
        </nav>

        <div>
            <table>
               <caption>DEMANDES</caption>
               <thead>
                   <tr>
                       <th>Nom</th>
                       <th>Prenom</th>
                       <th>Âge</th>
                       <th>Nom Reponsable</th>
                       <th>Prenom Responsable</th>
                       <th>Tel Reponsable</th>
                       <th>Date</th>
                       <th>Plage horaire</th>
                       <th><input type=button onClick="location.href='adminValiderEnfant.jsp" value='Accepter'></th>
                   </tr>
               </thead>
        </div>
    </body>
</html>


