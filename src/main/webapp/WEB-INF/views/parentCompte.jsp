<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Compte parent</h1>
        </nav>
        <div>
            <div id="parentChild">
                <!--should retrieve parent's childs here and show them right there -->
                <h2>Mes enfants</h2>
                <form>
                    <p>Marmot 1</p><input type="checkbox">
                    <p>Marmot 2</p><input type="checkbox">
                </form>
                <a href="/log-in/parentCompte/ajouterEnfant">Ajouter un enfant</a>
            </div>
            <div id="creneauxEnfant">
                <h2> Creneaux </h2>
                <form>
                    <input type="date">
                    <p>De : </p><input type="time">
                    <p> A : </p><input type="time">
                </form>
            </div>
            <div id="lienGarderie">
                <a href="/log-in/parentCompte/ajoutHabilite">Ajouter une personne habilitée</a><br/>
                <a href="/log-in/parentCompte/parentListeDemande">Liste demandes</a>
            </div>
            <div id="accountAction">
                <form>
                    <input type="button" value="Signaler absence" onclick="location.href='/parentAlerte">
                    <!--<input type="button" value="Logout" onclick="location.href=\"<c:url value='/logout'/>\"">-->
                    <a href="<c:url value='/logout'/>">Déconnexion</a>
                </form>
            </div>
            <!--<a href="/log-in/parentCompte/parentListeDemande">Liste demandes</a>
            <br />
            <a href="/log-in/parentCompte/ajouterEnfant">Ajouter un enfant</a>
            <br />
            <a href="/log-in/parentCompte/ajoutHabilite">Ajouter une personne habilitée</a>
            <br />
            <a href="/log-in/parentCompte/parentAlerte">Alerte</a>
            -->
        </div>
    </body>
</html>