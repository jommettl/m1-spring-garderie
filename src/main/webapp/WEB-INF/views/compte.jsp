<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compte parent</title>
        <link href="<c:url value="/resources/css/compte.css" />" rel="stylesheet">
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Compte</h1>
        </nav>
        <div>
            <a href="enfants">Mes enfants </a><br/>
            <a href="planning">Mon planning </a><br/>
            <a href="demandes">Mes demandes/Résultats </a><br/>
            <a href="habilitation">Personnes habilitées </a><br/>
            <a href="facture">Mes factures </a><br/>
            <a href="absences">Signaler absences </a><br/>
        </div>
    </body>
</html>



