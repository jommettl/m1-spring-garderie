<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compte administrateur</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
        <%--<script type='text/javascript' src="<c:url value="/resources/js/app.js" />"></script> --%>

    </head>

    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Compte administrateur</h1>
        </nav>

        <aside>
            <a href="/log-in/adminCompte/adminValiderEnfant">Valider un enfant</a>
            <br />
            <a href="/log-in/adminCompte/adminAlerte">Alerte (v_admin)</a>
            <br/>
            <!--<a href="<c:url value="/logout"/>"> Logout </a>-->
            <a href="<c:url value='/logout'/>">Déconnexion</a>

        </aside>

    </body>

</html>


