<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test</title>
        <link href="<c:url value="/resources/css/yolo.css" />" rel="stylesheet">
        <script type='text/javascript' src="<c:url value="/resources/js/JSonReader.js" />"></script>
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Test</h1>
        </nav>
    </body>
</html>