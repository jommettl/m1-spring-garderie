<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Accueil</title>
        <link href="<c:url value="/resources/css/accueil.css" />" rel="stylesheet">
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Accueil</h1>
        </nav>
        <aside id="bloc_accueil_news">
            <c:out value="${model.customer.all()}"/>
        </aside>
    </body>
</html>