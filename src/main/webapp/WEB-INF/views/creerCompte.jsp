<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Créer compte</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Créer compte</h1>
        </nav>
        <div>
            <form method="post" action="creerCompte">
                <label for="prenom">Quel est votre prenom ?</label>
                <p><input type="text" name="prenom" /></p>
                <label for="nom">Quel est votre nom ?</label>
                <p><input type="text" name="nom" /></p>
                <label for="adresse_email">Quel est votre adresse email ?</label>
                <p><input type="text" name="email" /></p>
                <label for="adresse">Quel est votre adresse ?</label>
                <p><input type="text" name="adresse" /></p>
                <label for="numero_telephone">Quel est votre numéro de téléphone ?</label>
                <p><input type="text" name="tel" /></p>
                <label for="sexe">Sexe ?</label>
                <p><input type= "radio" name="sexe" value="H"> H
                <input type= "radio" name="sexe" value="F"> F </p>
                <label for="pseudo">Pseudo</label>
                <p><input type="text" name="pseudo" /></p>
                <label for="psw">Mot de passe</label>
                <p><input type="password" name="psw"></p>
            </form>
            <form>
                <input type="submit" value="Envoyer" />
            </form>
        </div>
    </body>
</html>