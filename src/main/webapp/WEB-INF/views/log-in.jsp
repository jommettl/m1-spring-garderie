<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
        <title>Log-in</title>
        <%--<script type='text/javascript' src="<c:url value="/resources/js/app.js" />"></script> --%>
    </head>

    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Login</h1>
        </nav>
        <div>
            <form name='f' action="login" method="POST">
                <input type="text" name="username" value="">
                <input type="password" name="password">
                <input type="submit" value="Envoyer">
            </form>
            <!--<a href="/log-in/creerCompte">Creer compte </a>
            <br />
            <a href="/log-in/parentCompte">Compte parent</a>
            <br />
            <a href="/log-in/adminCompte">Compte admin</a> -->
            <form method="POST" action="creerCompte">
                <label for="submitCreerCompte">Créer compte</label>
                <input type="submit" name="submitCreerCompte" value="Creer compte">
            </form>
            <!-- <form method="POST" action="parentCompte">
                <label for="submitParentCompte">Compte parent</label>
                <input type="submit" name="submitParentCompte" value="Compte (parent)">
            </form>
            <form method="POST" action="adminCompte">
                <label for="submitAdminCompte">Compte admin</label>
                <input type="submit" name="submitAdminCompte" value="Compte (admin)">
            </form>-->
        </div>
    </body>
</html>



