<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alerte parent</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Parent alerte</h1>
        </nav>
        <div>
            <div>
                <form>
                    <input type="text" id="childName" placeholder="Nom de l'enfant"/><br/>
                    <input type="text" id="reason" placeholder="raison de l'absence" /><br/>
                    <input type="date" id="time"/><br/>
                    <input type="submit" value="Valider"/>
                </form>
            </div>
        </div>
    </body>
</html>