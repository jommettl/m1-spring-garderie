<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calendrier</title>
        <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    </head>
    <body>
        <nav>
            <%@include file="menu.jsp" %>
            <h1>Calendrier</h1>
        </nav>
        <table>
            <h1>Mois actuel</h1>
        </table>
        <br/>
        <table>
            <h1>Mois suivant</h1>
        </table>
    </body>
</html>