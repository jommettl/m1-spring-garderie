package fr.univ_smb.isc.m2.model;

import sun.security.util.Password;

public class Admin extends Utilisateur {
    Calendrier calendrier;
    Admin(String n, String p, int a, String log, Password pass, Calendrier c) {
        super(n, p, a, log, pass);
        calendrier = c;
    }

    void acceptEvent(Event e) throws Exception {
        e.acceptEvent();
    }

    void refuseEvent(Event e){
        e.warnParent();
    }
}
