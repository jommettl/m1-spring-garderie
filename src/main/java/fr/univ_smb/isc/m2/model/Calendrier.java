package fr.univ_smb.isc.m2.model;

public class Calendrier {
    int year;
    Month current;
    Month next;

    Calendrier(int n, int y, EnumDay begin, EnumMonth m) {
        current = new Month(begin, m);
        try {
            next = new Month(current.lastDay().next(), m.next());
        }catch(Exception e){
            e.printStackTrace();
        }
        year = y;
    }
}
