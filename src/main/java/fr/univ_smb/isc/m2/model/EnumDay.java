package fr.univ_smb.isc.m2.model;

public enum EnumDay {
    LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI, SAMEDI, DIMANCHE;

    EnumDay next() throws Exception {
        switch (this) {
            case LUNDI:
                return MARDI;
            case MARDI:
                return MERCREDI;
            case MERCREDI:
                return JEUDI;
            case JEUDI:
                return VENDREDI;
            case VENDREDI:
                return SAMEDI;
            case SAMEDI:
                return DIMANCHE;
            case DIMANCHE:
                return LUNDI;
            default:
                throw new Exception("Le jour demandé n'est pas existant");
        }

    }
}