package fr.univ_smb.isc.m2.model;

import java.util.List;

public class Child extends Personne{
    Parent[] parent = new Parent[2];
    List<Habilite> habilites;

    Child(String n, String p, int a){
        super(n, p, a);
    }

    void addParent(Parent p){
        if (parent[0] == null){
            parent[0] = p;
        }else if(parent[1] == null){
            parent[1] = p;
        }else{
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public void addHabilete(Habilite h) {
        habilites.add(h);
    }
}
