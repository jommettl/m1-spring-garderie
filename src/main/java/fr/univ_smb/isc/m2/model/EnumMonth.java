package fr.univ_smb.isc.m2.model;


/**
 * Created by eratotin on 28/11/16.
 */
public enum EnumMonth {
    JANVIER, FEVRIER, MARS, AVRIL, MAI, JUIN, JUILLET, AOUT, SEPTEMBRE, OCTOBRE, NOVEMBRE, DECEMBRE;

    EnumMonth next() throws Exception {
        switch (this) {
            case JANVIER:
                return FEVRIER;
            case FEVRIER:
                return MARS;
            case MARS:
                return AVRIL;
            case AVRIL:
                return MAI;
            case MAI:
                return JUIN;
            case JUIN:
                return JUILLET;
            case JUILLET:
                return AOUT;
            case AOUT:
                return SEPTEMBRE;
            case SEPTEMBRE:
                return OCTOBRE;
            case OCTOBRE:
                return NOVEMBRE;
            case NOVEMBRE:
                return DECEMBRE;
            case DECEMBRE:
                return JANVIER;
            default:
                throw new Exception("Le mois demandé n'est pas présent");
        }


    }

    public int getLength() throws Exception {
        switch (this) {
            case JANVIER:
                return 31;
            case FEVRIER:
                return 28;
            case MARS:
                return 31;
            case AVRIL:
                return 30;
            case MAI:
                return 31;
            case JUIN:
                return 30;
            case JUILLET:
                return 31;
            case AOUT:
                return 31;
            case SEPTEMBRE:
                return 30;
            case OCTOBRE:
                return 31;
            case NOVEMBRE:
                return 30;
            case DECEMBRE:
                return 31;
            default:
                throw new Exception("Le mois demandé n'est pas présent");
        }
    }
}
