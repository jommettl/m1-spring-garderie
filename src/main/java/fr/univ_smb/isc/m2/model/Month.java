package fr.univ_smb.isc.m2.model;

public class Month {
    EnumMonth month;
    Day[] jours;

    public Month(int n) {
        jours = new Day[n];
    }

    public Month(EnumDay begin, EnumMonth m) {
        try {
            jours = new Day[m.getLength()];
        }catch (Exception e){
            e.printStackTrace();
        }
        month = m;
        for (int i = 0; i < jours.length; i++){
            jours[i] = new Day(begin);
            try {
                begin = begin.next();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public EnumDay lastDay() {
        return jours[jours.length - 1].name();
    }
}
