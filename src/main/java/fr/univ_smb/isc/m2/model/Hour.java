package fr.univ_smb.isc.m2.model;


public class Hour {
    int hour;
    int minutes;

    Hour(int h, int m){
        hour = h + (m/60); //on ne test pas si heure > 24, risque de problème
        minutes = m%60;
    }

    boolean under(Hour h){
        if (h.hour == hour){
            return h.minutes < minutes;
        }else{
            return hour < h.hour;
        }
    }

    boolean over(Hour h){
        if (h.hour == hour){
            return h.minutes > minutes;
        }else{
            return hour > h.hour;
        }
    }
}
