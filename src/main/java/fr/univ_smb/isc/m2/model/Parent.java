package fr.univ_smb.isc.m2.model;

import sun.security.util.Password;

import java.util.List;

public class Parent extends Utilisateur{
    List<Child> children;
    List<Alert> alerts;
    Parent(String n, String p, int a, String log, Password pass) {
        super(n, p, a, log, pass);
    }

    void addChild(Child c){
        children.add(c);
    }

    void addHabilete(Habilite h, Child c) {
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).equals(c)){
                children.get(i).addHabilete(h);
            }
        }
    }

    public void warn(Event event) {
        alerts.add(new Alert(event));
    }

    public List<Alert> getAlerts(){
        return alerts;
    }

    public List<Child> getChildren(){
        return children;
    }
}
