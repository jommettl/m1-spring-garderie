package fr.univ_smb.isc.m2.model;

import java.util.List;

public class Day {
    EnumDay name;
    Hour openning, ending;
    List<Event> events;

    Day(EnumDay n, Hour o, Hour e){
        name = n;
        openning = o;
        ending = e;
    }

    public Day(EnumDay begin) {
        name = begin;
        openning = new Hour(0,0);
        ending = new Hour(0,0);
    }

    public void setOpenning(int hour, int minutes){
        openning = new Hour(hour, minutes);
    }

    public void setEnding(int hour, int minutes){
        ending = new Hour(hour, minutes);
    }

    public EnumDay name() {
        return name;
    }

    void addEvent(Event e) throws Exception {
        if (e.begin.under(openning) || e.ending.over(ending)){
            throw NotAllowedTime("Le temps fournis n'est pas ok");
        }else{
            events.add(e);
        }
    }

    private Exception NotAllowedTime(String s) {
        System.err.println(s);
        return null;
    }
}
