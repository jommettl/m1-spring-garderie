package fr.univ_smb.isc.m2.model;

import sun.security.util.Password;

public abstract class Utilisateur extends Personne{
    String login;
    Password password;

    Utilisateur(String n, String p, int a, String log, Password pass) {
        super(n, p, a);
        login = log;
        password = pass;
    }
}
