package fr.univ_smb.isc.m2.model;

public abstract class Personne {
    String nom;
    String prenom;
    int age;

    Personne(String n, String p, int a){
        nom = n;
        prenom = p;
        age = a;
    }
}
