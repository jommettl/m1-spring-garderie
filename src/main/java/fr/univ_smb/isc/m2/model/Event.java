package fr.univ_smb.isc.m2.model;

public class Event {
    EventDate eventDate;
    public Hour begin;
    public Hour ending;
    Child child;
    Personne whoComes;

    Event(Hour b, Hour e, Child c, Personne wc){
        begin = b;
        ending = e;
        child = c;
        whoComes = wc;
    }

    void acceptEvent() throws Exception {
        eventDate.month.jours[eventDate.day + 1].addEvent(this);
    }

    public void warnParent() {
        child.parent[0].warn(this);
        child.parent[1].warn(this);
    }
}
