package fr.univ_smb.isc.m2.controllers.web.controllers.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "accueil";
    }

    @RequestMapping("/accueil")
    public String accueil() {
        return "accueil";
    }

    @RequestMapping("/menu")
    public String menu(HttpServletRequest request) {
        if (request.isUserInRole("ADMIN")) {
            return "menu-admin";
        } else {
            return "menu";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "log-in";
    }

    @RequestMapping("/info")
    public String info() {
        return "info";
    }

    @RequestMapping("/calendar")
    public String calendar(HttpServletRequest request) {
        return "calendar";
    }

    @RequestMapping("/creerCompte")
    public String creerCompte(HttpServletRequest request) {
        if (request.getRemoteUser() == null) {
            return "creerCompte";
        } else {
            return "accueil";
        }
    }

    @RequestMapping("/log-in")
    public String login(HttpServletRequest request) {
        if (request.isUserInRole("ADMIN")) {
            return "adminCompte";
        } else {
            return "parentCompte";
        }
    }

    @RequestMapping("/log-in/parentCompte/ajouterEnfant")
    public String ajouterEnfant() {
        return "ajouterEnfant";
    }

    @RequestMapping("/log-in/parentCompte/ajoutHabilite")
    public String ajoutHabilite() {
        return "ajoutHabilite";
    }

    @RequestMapping("/log-in/parentCompte/parentListeDemande")
    public String parentListeDemande() {
        return "parentListeDemande";
    }

    @RequestMapping("/log-in/parentCompte/parentAlerte")
    public String parentAlerte() {
        return "parentAlerte";
    }

    @RequestMapping("/log-in/adminCompte/adminValiderEnfant")
    public String adminValiderEnfant() {
        return "adminValiderEnfant";
    }

    @RequestMapping("/log-in/adminCompte/adminAlerte")
    public String adminAlerte() {
        return "adminAlerte";
    }
}